package com.aeon.assignment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.aeon.assignment.feedback.FeedbackActivity
import com.aeon.assignment.sos.SosMainActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {

    //private val sos: Button? = null
    //private var feedback: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sos = findViewById<Button>(R.id.sos)
        sos.setOnClickListener(this)
        val feedback = findViewById<Button>(R.id.feedback)
        feedback.setOnClickListener(this)


    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.sos -> {
                val intent = Intent(applicationContext, SosMainActivity::class.java)
                startActivity(intent)
            }
            R.id.feedback -> {
                val intent = Intent(applicationContext, FeedbackActivity::class.java)
                startActivity(intent)
            }

        }
    }
}
