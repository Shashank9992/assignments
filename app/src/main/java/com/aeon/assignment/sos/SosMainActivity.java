package com.aeon.assignment.sos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.aeon.assignment.R;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class SosMainActivity extends AppCompatActivity {


    SharedPreferencesSoS sharedPreferencesSoS;


    private List<SosContactModel> sosContactModelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SosContactAdapter sosContactAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos_main);


        requestForPremission();
        initView();


        recyclerView = findViewById(R.id.recycler_view);

        sosContactAdapter = new SosContactAdapter(sosContactModelList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(sosContactAdapter);


        prepareSosContactList();

    }

    private void requestForPremission() {

        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Toast.makeText(SosMainActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(SosMainActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT)
                        .show();
            }


        };

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.READ_CONTACTS, Manifest.permission.SEND_SMS)
                .check();

    }

    private void sendAlert() {


        new AlertDialog.Builder(SosMainActivity.this)
                .setTitle("Alert")
                .setMessage("Are you sure you want to send SoS")

                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        SmsManager shortMessageManager;
                        shortMessageManager = SmsManager.getDefault();


                        String message = "Hello from Android";
                        try {
                            // Do something
                            for (int i = 0; i < sosContactModelList.size(); i++) {
                                Thread.sleep(3000);
                                shortMessageManager.sendTextMessage(sosContactModelList.get(i).getContactNumber(),
                                        null, message, null, null);
                                Log.e("PhoneNo", sosContactModelList.get(i).getContactNumber() + "-->");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("PhoneNo", "fail");
                        }

                    }
                })

                .setNegativeButton(android.R.string.no, null)
                .show();


    }

    private void initView() {

        sharedPreferencesSoS = new SharedPreferencesSoS(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }


    private void prepareSosContactList() {


        if (!sosContactModelList.isEmpty()) {
            sosContactModelList.clear();
        }


        try {
            String jsonString = sharedPreferencesSoS.PickValue(SharedPreferencesSoS.arrayValue);
            JSONArray jsonArray = new JSONArray(jsonString);


            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                Iterator<String> keys = json.keys();

                while (keys.hasNext()) {
                    String key = keys.next();
                    //  System.out.println("Key :" + key + "  Value :" + json.get(key));
                    String value = String.valueOf(json.get(key));

                    String[] namesList = value.split(",");


                    SosContactModel sosContactModel = new SosContactModel(key, namesList[0], namesList[1]);
                    sosContactModelList.add(sosContactModel);
                }

            }

            sosContactAdapter.notifyDataSetChanged();


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(SosMainActivity.this, "You did't add any contact yet",
                    Toast.LENGTH_LONG).show();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.manage_sos_contact) {

            Intent intent = new Intent(SosMainActivity.this, AddSoSContact.class);
            startActivity(intent);

            return true;
        }

        if (item.getItemId() == R.id.send_sos_contact) {

            if (sosContactModelList.size() == 0) {
                Toast.makeText(SosMainActivity.this, "Please add contact in sos",
                        Toast.LENGTH_LONG).show();

            } else {
                sendAlert();
            }

            return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        prepareSosContactList();
    }
}
