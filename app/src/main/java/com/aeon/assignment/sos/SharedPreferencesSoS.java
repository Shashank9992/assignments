package com.aeon.assignment.sos;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SharedPreferencesSoS {

    public static final String SosPreferences = "MyPrefs";
    public static final String arrayValue = "arrayValue";


    Context _session_acti;
    SharedPreferences sharedpreferences;

    public SharedPreferencesSoS(Context _session_acti) {
        super();
        this._session_acti = _session_acti;
    }

    public void StoreFirstPreference(String jsonArrayData) {
        Log.e("jsonArrayData",jsonArrayData+"");
        sharedpreferences = _session_acti.getSharedPreferences(SosPreferences,
                Context.MODE_PRIVATE);
        Editor editor = sharedpreferences.edit();
        editor.putString(arrayValue, jsonArrayData);
        editor.commit();
    }


    public void SessionClear() {

        SharedPreferences sharedpreferences = _session_acti
                .getSharedPreferences(SosPreferences, Context.MODE_PRIVATE);
        Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }



    public String PickValue(String Key) {
        String output = "";
        sharedpreferences = _session_acti.getSharedPreferences(SosPreferences,
                Context.MODE_PRIVATE);
        output = sharedpreferences.getString(Key, null);
        return output;
    }



}
