package com.aeon.assignment.sos;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.aeon.assignment.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;

public class AddSoSContact extends AppCompatActivity implements View.OnClickListener {


    Toolbar toolbar;
    ImageView contactIcon;
    EditText contactName, contactMobile;
    Button save;


    SharedPreferencesSoS sharedPreferencesSoS;

    String valueCheck;

    JSONObject hashMap = new JSONObject();

    String key = "0";

    String idForEdit, nameForEdit, numberForEdit;

    boolean edit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sos_contact);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            idForEdit = bundle.getString("id");
            nameForEdit = bundle.getString("name");
            numberForEdit = bundle.getString("number");

            edit = true;
        } else {
            edit = false;
        }


        //all the findViewById inside this
        initView();

        //to get the value from Shared Preferences
        checkSharedPreferences();

        Log.e("valueCheck", valueCheck + "-->");


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void checkSharedPreferences() {


        try {
            String jsonString = sharedPreferencesSoS.PickValue(SharedPreferencesSoS.arrayValue);
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {

                valueCheck = String.valueOf(jsonArray.length());

                JSONObject json = jsonArray.getJSONObject(i);
                Iterator<String> keys = json.keys();

                while (keys.hasNext()) {
                    String key = keys.next();
                    System.out.println("Key :" + key + "  Value :" + json.get(key));
                    String value = String.valueOf(json.get(key));

                    try {
                        hashMap.put(key, value);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    final JSONArray jsonArrayMap = new JSONArray();
                    jsonArrayMap.put(hashMap);


                    Log.e("value", jsonArrayMap + "--->");

                }

            }


        } catch (JSONException e) {
            e.printStackTrace();


        } catch (Exception e) {
            e.printStackTrace();
            valueCheck = "0";

        }

    }

    private void initView() {

        sharedPreferencesSoS = new SharedPreferencesSoS(this);

        save = findViewById(R.id.save);
        save.setOnClickListener(this);
        toolbar = findViewById(R.id.toolbar);
        contactName = findViewById(R.id.contactName);
        contactMobile = findViewById(R.id.contactMobile);
        contactIcon = findViewById(R.id.contactIcon);
        contactIcon.setOnClickListener(this);

        if (edit){
            contactName.setText(nameForEdit);
            contactMobile.setText(numberForEdit);
        }

    }

    @Override
    public void onClick(View v) {
        // Perform action on click
        switch (v.getId()) {

            case R.id.contactIcon:
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(i, 101);
                break;

            case R.id.save:

                validateField();

                break;
        }

    }

    private void validateField() {
        if (contactName.getText().toString().length() == 0) {
            contactName.setError("name can't be empty");
        } else if (contactMobile.getText().toString().length() < 10) {
            contactMobile.setError("invalid mobile number");
        } else {
            Log.e("valueCheck on save", valueCheck);
            saveContactForSos();
        }
    }

    private void saveContactForSos() {
        JSONObject hashMapNew = new JSONObject();
        int keyNew = 0;
        try {
            String jsonString = sharedPreferencesSoS.PickValue(SharedPreferencesSoS.arrayValue);
            JSONArray jsonArray = new JSONArray(jsonString);


            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                Iterator<String> keys = json.keys();

                while (keys.hasNext()) {
                    key = keys.next();
                    System.out.println("Key :" + key + "  Value :" + json.get(key));
                    String value = String.valueOf(json.get(key));

                    hashMapNew.put(key, value);

                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();

        }

        if (edit){
            keyNew = Integer.parseInt(idForEdit);
        }else {
            keyNew = Integer.parseInt(key) + 1;
        }

        /*if (DataBridge.id.equalsIgnoreCase("") || DataBridge.id == null) {
            keyNew = Integer.parseInt(key) + 1;
        } else {
            keyNew = Integer.parseInt(DataBridge.id);
        }*/


        try {
            hashMapNew.put(String.valueOf(keyNew), contactName.getText().toString() + "," + contactMobile.getText().toString());

            final JSONArray jsonArray = new JSONArray();
            jsonArray.put(hashMapNew);
            sharedPreferencesSoS.StoreFirstPreference(String.valueOf(jsonArray));

            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {
            Uri contactUri = data.getData();
            Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);
            cursor.moveToFirst();
            int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            String name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
            String mobile = cursor.getString(column).trim();

            setValue(name, mobile);

        }
    }

    @SuppressLint("SetTextI18n")
    private void setValue(String name, String mobile) {
        contactName.setText(name + "");
        contactMobile.setText(mobile + "");
    }


}
