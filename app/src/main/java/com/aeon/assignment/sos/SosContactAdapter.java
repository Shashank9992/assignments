package com.aeon.assignment.sos;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.aeon.assignment.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

public class SosContactAdapter extends RecyclerView.Adapter<SosContactAdapter.MyViewHolder> {

    private List<SosContactModel> sosContactModelList;
    Activity activity;
    SharedPreferencesSoS sharedPreferencesSoS;
    String key;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, mobile, delete, edit;

        MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            mobile = view.findViewById(R.id.mobile);
            delete = view.findViewById(R.id.delete);
            edit = view.findViewById(R.id.edit);
        }
    }


    SosContactAdapter(List<SosContactModel> sosContactModelList, Activity activity) {
        this.sosContactModelList = sosContactModelList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sos_contact, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SosContactModel sosContactModel = sosContactModelList.get(position);
        holder.name.setText(sosContactModel.getContactName());
        holder.mobile.setText(sosContactModel.getContactNumber());

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, AddSoSContact.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", sosContactModel.getId());
                bundle.putString("name", sosContactModel.getContactName());
                bundle.putString("number", sosContactModel.getContactNumber());
                intent.putExtras(bundle);
                activity.startActivity(intent);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferencesSoS = new SharedPreferencesSoS(activity);

                new AlertDialog.Builder(activity)
                        .setTitle("Remove contact")
                        .setMessage("Are you sure you want to delete this contact?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                deleteContactForSos(sosContactModel.getId(),position);
                            }
                        })

                        .setNegativeButton(android.R.string.no, null)
                        .show();


            }
        });

    }

    @Override
    public int getItemCount() {
        return sosContactModelList.size();
    }


    private void deleteContactForSos(String idToRemoved,int position)
    {
        JSONObject hashMapNew = new JSONObject();

        try {
            String jsonString = sharedPreferencesSoS.PickValue(SharedPreferencesSoS.arrayValue);
            JSONArray jsonArray = new JSONArray(jsonString);


            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                Iterator<String> keys = json.keys();

                while (keys.hasNext()) {
                    key = keys.next();
                    System.out.println("Key :" + key + "  Value :" + json.get(key));
                    String value = String.valueOf(json.get(key));

                    hashMapNew.put(key, value);
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        try {

            hashMapNew.remove(idToRemoved);
            final JSONArray jsonArray = new JSONArray();
            jsonArray.put(hashMapNew);
            sharedPreferencesSoS.StoreFirstPreference(String.valueOf(jsonArray));

            sosContactModelList.remove(position);
            notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}